<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Document;
use AppBundle\Repository\DocumentRepository;
use Spipu\Html2Pdf\Html2Pdf;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\CssSelector\Exception\InternalErrorException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotAcceptableHttpException;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class DefaultController
 *
 * @package AppBundle\Controller
 */
class DefaultController extends Controller
{
    /**
     * @Route("/", name="documentsList")
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function listAction()
    {
        $em = $this->getDoctrine()->getManager();

        /** @var DocumentRepository $repository */
        $repository = $em->getRepository(Document::class);

        $documents = $repository->findAll();

        return $this->render(
            'default/list.html.twig',
            array(
                'documents' => $documents
            )
        );
    }

    /**
     * @Route("/new", name="newDocument")
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function newDocumentAction()
    {
        return $this->render('default/document.html.twig', array('isEdit' => false));
    }

    /**
     * @param $id
     *
     * @Route("/edit/{id}", name="editDocument")
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function editDocumentAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        /** @var DocumentRepository $repository */
        $repository = $em->getRepository(Document::class);

        /** @var Document $document */
        $document = $repository->find($id);
        if(!$document){
            throw $this->createNotFoundException();
        }

        return $this->render(
            'default/document.html.twig',
            array_merge(
                array(
                    'isEdit' => true,
                    'id' => $document->getId(),
                    'title' => $document->getTitle(),
                ),
                json_decode($document->getData(), true)
            )
        );
    }

    /**
     * @param $id
     *
     * @Route("/print/{id}", name="printDocument")
     *
     * @throws \Spipu\Html2Pdf\Exception\Html2PdfException
     * @throws InternalErrorException
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function printDocumentAction($id, Request $request)
    {
        ob_start();

        $em = $this->getDoctrine()->getManager();

        /** @var DocumentRepository $repository */
        $repository = $em->getRepository(Document::class);

        /** @var Document $document */
        $document = $repository->find($id);
        if(!$document){
            throw $this->createNotFoundException();
        }

        if(!$request->query->has('format')) {
            throw new NotAcceptableHttpException('No format presented');
        }

        $format = $request->query->get('format');

        if($format < 0 || $format > 6) {
            throw new NotAcceptableHttpException('Unsupported format');
        }

        $html = $this->render(
            'print/template.html.twig',
            array_merge(array('format' => $format), json_decode($document->getData(), true))
        )->getContent();

        $pdf = new Html2Pdf('P', $format, 'en', true, 'UTF-8', [ 5, 5, 5, 8 ], false);
        $pdf->writeHTML($html);

        return new Response(
            $pdf->output($document->getTitle() . '.pdf'),
            Response::HTTP_OK,
            [ 'Content-Type' => 'application/pdf' ]
        );
    }
}
