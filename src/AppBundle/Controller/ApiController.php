<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Document;
use AppBundle\Repository\DocumentRepository;
use Spipu\Html2Pdf\Html2Pdf;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\CssSelector\Exception\InternalErrorException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotAcceptableHttpException;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class ApiController
 *
 * @package AppBundle\Controller
 */
class ApiController extends Controller
{
    /**
     * @Route("/api/save", name="saveDocument")
     *
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function saveAction( Request $request )
    {
        $em = $this->getDoctrine()->getManager();
        /** @var DocumentRepository $repository */
        $repository = $em->getRepository(Document::class);

        $parameters = $request->request;

        $document = new Document();
        if($parameters->has('id')) {
            /** @var Document $document */
            $document = $repository->find($parameters->get('id'));
            $document->setUpdatedAt(new \DateTime());
        }

        $document->setTitle($parameters->get('title'));
        $document->setData($parameters->get('data'));
        $document->setIsActive($parameters->has('is_active') ? $parameters->getBoolean('is_active') : true);

        $em->persist($document);
        $em->flush();

        return $this->json(array('success' => true, 'id' => $document->getId()));
    }

    /**
     * @Route("/api/activity/toggle", name="toggleActivityAction")
     *
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function toggleActivityAction( Request $request )
    {
        $em = $this->getDoctrine()->getManager();
        /** @var DocumentRepository $repository */
        $repository = $em->getRepository(Document::class);

        $parameters = $request->request;

        $success = $parameters->has('id');
        $isActive = false;

        if($success) {
            /** @var Document $document */
            $document = $repository->find($parameters->get('id'));

            $success = !is_null($document);
            if($success) {
                $document->setIsActive(!$document->isActive());
                $document->setUpdatedAt(new \DateTime());

                $em->persist($document);
                $em->flush();

                $isActive = $document->isActive();
            }
        }

        return $this->json(array('success' => $success, 'is_active' => $isActive));
    }
}
