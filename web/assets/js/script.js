$(document).ready(function () {
    // List

    let printButton = $('button#togglePrintModal');
    printButton.on('click', function () {
        let documentIdentifier = $(this).data('document-id');
        let reference = '/print/'+documentIdentifier;

        let modalContainer = $('#printModal');
        modalContainer.modal('show');

        let pageFormatSelector = modalContainer.find('select#pageFormat');
        let downloadLink = modalContainer.find('a#download-pdf');

        let pageFormat = pageFormatSelector.val();
        let finalReference = reference + '?format='+pageFormat;
        downloadLink.attr('href', finalReference);

        pageFormatSelector.on('change', function () {
            pageFormat = $(this).val();
            finalReference = reference + '?format='+pageFormat;
            downloadLink.attr('href', finalReference);
        });
    });

    $('button#removeDocument').on('click', function () {
        let documentId = $(this).data('document-id');
        let request = $.ajax({
            url: "/api/activity/toggle",
            method: "POST",
            data: {'id': documentId}
        });

        let button = $('button.toggle-'+documentId);

        request.done(function (data) {
            if(data.success) {
                let printButton = $('button.print-'+documentId);
                let editButton = $('a.edit-'+documentId);

                if (data.is_active) {
                    editButton.removeClass('disabled');
                    printButton.removeAttr('disabled');
                } else {
                    printButton.attr('disabled', 'disabled');
                    editButton.addClass('disabled');
                }

                $('button.toggle-'+documentId)
                    .removeClass('btn-' + (data.is_active ? 'success' : 'danger'))
                    .addClass('btn-' + (data.is_active ? 'danger' : 'success'))
                    .html(data.is_active ? 'Deactivate' : 'Activate');

                $('td.active-'+documentId).html(data.is_active ? 'Yes' : 'No');
            }

        })
    });

    // Form

    let form = $("form#documentForm");

    form.find("button.save").on("click", function (event) {
        event.preventDefault();

        let title = form.find("input#title").val();

        let data = {};
        for (let i = 1; i < 512; i++){
            data["field_"+i] = form.find("input#field_"+i).val();
        }

        let postData = {
            title: title,
            data: JSON.stringify(data)
        };

        let identifier = form.find("input#identifier").val();
        if(identifier > 0) {
            postData['id'] = identifier;
        }

        if(title && title.length > 0) {
            let request = $.ajax({
                url: "/api/save",
                method: "POST",
                data: postData,
            });

            request.done(function () {
                let successAlert = form.find("div#success");
                successAlert.css('display', 'block');
                setTimeout(function () {
                    successAlert.css('display', 'none');
                }, 2500)
            });

            request.fail(function () {
                console.log("error");
            });
        }
    });
});
