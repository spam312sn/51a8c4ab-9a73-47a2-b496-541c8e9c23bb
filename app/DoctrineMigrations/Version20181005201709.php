<?php

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\DBAL\Types\Type;
use Doctrine\Migrations\AbstractMigration;

/**
 * Class Version20181005201709
 *
 * @package Application\Migrations
 */
final class Version20181005201709 extends AbstractMigration
{
    const TABLE_NAME = 'documents';

    /**
     * @param Schema $schema
     *
     * @throws \Doctrine\DBAL\DBALException
     * @throws \Doctrine\DBAL\Migrations\AbortMigrationException
     */
    public function up(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $table = $schema->createTable(static::TABLE_NAME);
        $table->addColumn('id', Type::INTEGER, array('autoincrement' => true));
        $table->addColumn('title', Type::STRING);
        $table->addColumn('data', Type::TEXT);
        $table->addColumn('is_active', Type::BOOLEAN);
        $table->addColumn('created_at', Type::DATETIME);
        $table->addColumn('updated_at', Type::DATETIME, array('notnull' => false));
        $table->setPrimaryKey(array('id'));
    }

    /**
     * @param Schema $schema
     *
     * @throws \Doctrine\DBAL\DBALException
     * @throws \Doctrine\DBAL\Migrations\AbortMigrationException
     */
    public function down(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $schema->dropTable(static::TABLE_NAME);
    }
}
