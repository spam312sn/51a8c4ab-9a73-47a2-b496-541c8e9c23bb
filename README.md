### Project ID: `51a8c4ab-9a73-47a2-b496-541c8e9c23bb`

### Task description:

> Добрый день, Сергей,
>
> Ниже описание тестовой задачи:
> 
> Печать форм на A4/A5/A3
> 
> Форма ввода или показа информации на Web форме занимается JavaScript процедура в которую скармливается объект формы с методами показа и обработки ( например, Закрыть, Сохранить, Распечатать ).
> 
> В объекте формы есть Property – опции печати ( форматы ).
> 
> В Форме есть метод печати формы для каждой опции печати применяется свой шаблон преобразования HTML2PDF.
> 
> Должен быть прописан метод печати по-умолчанию.
> 
> Надо реализовать простую программку показа формы с возможностью, печати ( создания PDF размером A4, A5, A3 ). По умолчанию – A4. Тестовые данные должны быть на нескольких страницах ( PDF файлы получаемые д.б. многостраничные ).
> 
> На форме можно показать статические данные
> 
> Label1: Value1
> 
> Label2: Value2
> 
> Label3: Value3
> 
> . . .
> 
> Реализовать на PHP5.6 и Apache 2.4
> 
> Версию HTML2PDF возьмите последнюю стабильную.
> 
> Обдумайте план реализации и дайте знать если захотите его обсудить.
> 
> С вопросами обращайтесь по мере возникновения

`It is copy from original. All contacts, employer name and company name are removed in accordance with GDPR`

### Requirements

- PHP version: >= [5.5.9](https://symfony.com/doc/3.4/reference/requirements.html)
    - ext-json
- MySQL
- Composer
- Bower

### Installation

1. Clone project to `/var/www/`
2. `php composer.phar install`
3. `bower install`
4. `cp app/config/parameters.yml.dist app/config/parameters.yml`
5. `php bin/console doctrine:database:create`
6. `php bin/console doctrine:migrations:migrate`
7. `cp 51a8c4ab-9a73-47a2-b496-541c8e9c23bb.local.conf /etc/apache2/sites-available/`
8. `sudo a2ensite 51a8c4ab-9a73-47a2-b496-541c8e9c23bb.local.conf`
9. `sudo service apache2 reload`
10. `sudo service apache2 restart`
11. Add permissions for cache and logs repository
    ```
    HTTPDUSER=`ps aux | grep -E '[a]pache|[h]ttpd|[_]www|[w]ww-data|[n]ginx' | grep -v root | head -1 | cut -d\  -f1`
    sudo setfacl -R -m u:"$HTTPDUSER":rwX -m u:`whoami`:rwX var/cache var/logs
    sudo setfacl -dR -m u:"$HTTPDUSER":rwX -m u:`whoami`:rwX var/cache var/logs
    ```
12. Or if you have probles with `setfacl`:
    ```
    rm -rf var/cache/*
    rm -rf var/logs/*

    HTTPDUSER=`ps aux | grep -E '[a]pache|[h]ttpd|[_]www|[w]ww-data|[n]ginx' | grep -v root | head -1 | cut -d\  -f1`
    sudo chmod +a "$HTTPDUSER allow delete,write,append,file_inherit,directory_inherit" var/cache var/logs
    sudo chmod +a "`whoami` allow delete,write,append,file_inherit,directory_inherit" var/cache var/logs
    ```
### Running

1. Open your favourite browser
2. Go to `http://51a8c4ab-9a73-47a2-b496-541c8e9c23bb.local`
